const Dao = require("./dao.js");

module.exports = class PersonDao extends Dao {
  getAll(callback) {
    super.query("select navn, alder, adresse from personTest404", [], callback);
  }

  getOne(id, callback) {
    super.query("select navn, alder, adresse from personTest404 where id=?",
      [id],
      callback
    );
  }

  createOne(json, callback) {
    var val = [json.navn, json.adresse, json.alder];
    super.query("insert into personTest404 (navn,adresse,alder) values (?,?,?)",
      val,
      callback
    );
  }

    deleteOne(id, callback){
        super.query("DELETE FROM personTest404 WHERE id=?",
            [id],
            callback
        );
    }

    updateOne(json, id, callback){
        let navn = json.navn;
        let adresse = json.adresse;
        let alder = json.alder;

        if (json.navn.trim() === ""){
            navn = super.query("SELECT navn FROM personTest404 WHERE id=?", [id], callback);
        }
        if (json.adresse.trim() === ""){
            adresse = super.query("SELECT adresse FROM personTest404 WHERE id=?", [id], callback);
        }
        if(json.alder == null){
            alder = super.query("SELECT alder FROM personTest404 WHERE id=?", [id], callback);
        }

        let val = [navn, adresse, alder, id];
        super.query("UPDATE personTest404 SET navn=?, adresse=?, alder=? WHERE id=?",
            val,
            callback
        );
    }

};
